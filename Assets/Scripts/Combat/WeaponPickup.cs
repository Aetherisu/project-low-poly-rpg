﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ASRPG.Combat {
    public class WeaponPickup : MonoBehaviour
    {
        [SerializeField] Weapon weaponToEquip = null;
       private void OnTriggerEnter(Collider other) {
           if(other.gameObject.tag == "Player")
            {
                other.gameObject.GetComponent<Fighter>().EquipWeapon(weaponToEquip);
                Destroy(gameObject);
           }
 
       }
    }

}