using UnityEngine;
using ASRPG.Movement;
using ASRPG.Core;

namespace ASRPG.Combat {
public class Fighter : MonoBehaviour, IAction
    {
 
        [SerializeField]float timeBetweenAttacks = 1f;

        [SerializeField]Weapon defaultWeapon = null;
        [SerializeField]Transform handTransformRight = null;
        [SerializeField] Transform handTransformLeft = null;



        Health target;
        float timeSinceLastAttack = Mathf.Infinity;
        Mover mover;
        Weapon currentWeapon = null;

        private void Start() {
            mover = GetComponent<Mover>();
            EquipWeapon(defaultWeapon);
        }
        private void Update() {
            timeSinceLastAttack += Time.deltaTime;
            if(target == null) return;
            if(target.IsDead()) return;
            if( target != null && !IsInRange())
            {
                mover.MoveTo(target.transform.position);
                
            }
            else
            {
                mover.Cancel();
                AttackBehaviour();
            }
        }

        private void AttackBehaviour()
        {
            transform.LookAt(target.transform);
            if(timeSinceLastAttack > timeBetweenAttacks)
            {
                TriggerAttack();
                timeSinceLastAttack = 0;
            }
        }
        public void EquipWeapon(Weapon weapon)
        {
            currentWeapon = weapon;
            Animator animator = GetComponent<Animator>();
            weapon.Spawn(handTransformRight, handTransformLeft, animator);
         
        }
        private void TriggerAttack()
        {
            GetComponent<Animator>().ResetTrigger("attack");
            GetComponent<Animator>().SetTrigger("attack");
        }

        public bool CanAttack(GameObject combatTarget)
        {
            if(combatTarget == null) return false;
            Health testTargetting = combatTarget.GetComponent<Health>();
            return testTargetting != null && !testTargetting.IsDead();
        }
        public void Attack(GameObject combatTarget)
        {
            GetComponent<ActionScheduler>().StartAction(this);
      
            target = combatTarget.GetComponent<Health>();
        }
        private bool IsInRange()
        {
            return Vector3.Distance(target.transform.position, transform.position) < currentWeapon.GetRange();
        }
        public void Cancel()
        {
            StopAttack();
            target = null;
        }

        private void StopAttack()
        {
            GetComponent<Animator>().ResetTrigger("attack");
            GetComponent<Animator>().SetTrigger("stopAttack");
        }

        //Animation Event
        void Hit()
        {
            if (target == null) return;
            if(currentWeapon.HasProjectile())
            {
                currentWeapon.LaunchProjectile(handTransformRight, handTransformLeft, target);
            }
            else
            {
                target.TakeDamage(currentWeapon.GetDamage());
            }
           
        }
   
    }
}
