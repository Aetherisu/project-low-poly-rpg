﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ASRPG.Core;

namespace ASRPG.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] float projectileSpeed = 1f;
        [SerializeField] float projectileAim = 6f;
        [SerializeField] bool homingMissile = false;
        [SerializeField] GameObject hitEffect = null;
        [SerializeField] float maxAliveTime = 5f;
        [SerializeField] GameObject[] destroyOnHit = null;
        float timeAlive = 0f;
        Health target = null;
        float damage = 0;
        // Start is called before the first frame update

        private void Start()
        {
            transform.LookAt(GetAimLocation());
        }

        // Update is called once per frame
        void Update()
        {
            if (target == null) return;
            if(homingMissile && !target.IsDead()) transform.LookAt(GetAimLocation());
            transform.Translate(Vector3.forward * projectileSpeed * Time.deltaTime);
            timeAlive += Time.deltaTime;
            if(timeAlive > maxAliveTime)
            {
                Destroy(gameObject);
            }
        }
        public void SetTarget(Health target, float damage)
        {
            this.target = target;
            this.damage = damage;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Health>() == null) Destroy(gameObject);
            if (other.GetComponent<Health>() != target) return;
            if (target.IsDead()) return;
            target.TakeDamage(damage);
            if(hitEffect != null)
            {
                Instantiate(hitEffect, GetAimLocation(), transform.rotation);
            }

            foreach(GameObject toDestroy in destroyOnHit)
            {
                Destroy(gameObject);
            }
            Destroy(gameObject);
        }
        private Vector3 GetAimLocation()
        {
            CapsuleCollider targetCapsule = target.GetComponent<CapsuleCollider>();
            if (targetCapsule == null) return target.transform.position;
            return target.transform.position + Vector3.up * targetCapsule.height / projectileAim;
        }
    }

}
