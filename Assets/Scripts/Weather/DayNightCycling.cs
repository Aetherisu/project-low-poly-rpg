﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

namespace ASRPG.Weather
{
    public class DayNightCycling : MonoBehaviour
    {
        [SerializeField] float dayNightSpeed = 1f;
        [SerializeField] float lightIntensitySpeed = 5f;
        [SerializeField] float colourTemperatureSpeed = 100f;
        [SerializeField] float maxColourTemperature = 20000f;
        [SerializeField] float maxExposure = 4f;
        [SerializeField] float minExposure = -1f;
        [SerializeField] float exposureSpeed = .2f;
        [SerializeField] float maxLightIntensity = 15f;
        [SerializeField] float minLightIntensity = 1f;
        [SerializeField] VolumeProfile volumeProfile;

        float time = 1f;
        float lightIntensity = 0;
        float colourTemperature = 0;
        bool isDay = true;
        HDAdditionalLightData lightDataHD;
        Light lightData;
        GameObject[] worldLighting;


        private void Start()
        {
            lightDataHD = gameObject.GetComponent<HDAdditionalLightData>();
            lightData = gameObject.GetComponent<Light>();
            worldLighting = GameObject.FindGameObjectsWithTag("Dynamic Light");

        }
        void Update()
        {
            time += dayNightSpeed * Time.deltaTime;
      
            TimeOfDay();
            LightController();
            if (isDay)
            {
                DayTime();
            }
            if (!isDay)
            {
                NightTime();
            }

        }
        public void TimeOfDay()
        {

            switch (Mathf.Round(time))
            {
                case 15f:
                    print("08:00am");
                    foreach (GameObject light in worldLighting)
                    {
                        light.GetComponent<Light>().enabled = false;
                    }
            
                   
                    break;

                case 30f:
                    print("09:00am");
                    break;
                case 45f:
                    print("10:00am");
                    break;
                case 60f:
                    print("11:00am");

                    break;
                case 75f:
                    print("12:00pm");
                    break;
                case 90f:
                    print("13:00pm");
                    break;
                case 105f:
                    print("14:00pm");
                    break;
                case 120f:
                    print("15:00pm");
                    break;
                case 135f:
                    print("16:00pm");
                    break;
                case 150f:
                    print("17:00pm");
                    break;
                case 165f:
                    print("18:00pm");
                    break;
                case 180f:
                    print("19:00pm");
                    isDay = false; 
                    transform.eulerAngles = new Vector3(1, 0, 0);
                    break;
                case 195f:
                    print("20:00pm");
                    foreach (GameObject light in worldLighting)
                    {
                        light.GetComponent<Light>().enabled = true;
                    }
                    break;
                case 210f:
                    print("21:00pm");
                 
                    break;
                case 225f:
                    print("22:00pm");
                    break;
                case 240f:
                    print("23:00pm");
                    break;
                case 255f:
                    print("00:00am");
                    break;
                case 270f:
                    print("01:00am");
                    break;
                case 285f:
                    print("02:00am");
                    break;
                case 300f:
                    print("03:00am");
                    break;
                case 315f:
                    print("04:00am");
                    break;
                case 330f:
                    print("05:00am");
                    break;
                case 345f:
                    print("06:00am");
                    break;
                case 360f:
                    print("07:00am");
                    isDay = true; 
                    time = 1; 
                    transform.eulerAngles = new Vector3(1, 0, 0);
                    break;
                default:
                    break;
            }

        }
        void LightController()
        {
 
            volumeProfile.TryGet<Exposure>(out var exposure);
            if (time >= 0f && time <= 75f)
            {
                //lightIntensity += lightIntensitySpeed * dayNightSpeed * Time.deltaTime;
                colourTemperature += colourTemperatureSpeed * dayNightSpeed * Time.deltaTime;
                IntensityController();
                exposure.compensation.value += Mathf.Clamp(exposureSpeed * Time.deltaTime, minExposure, maxExposure);
            }
            else if(time >= 76f && time <= 255f)
            {
                lightIntensity -= lightIntensitySpeed * dayNightSpeed * Time.deltaTime;
          

                if (time >= 165f)
                {
                    colourTemperature = 20000;
                    exposure.compensation.value -= Mathf.Clamp(exposureSpeed * Time.deltaTime, minExposure, maxExposure);
                }
                else
                {
                    colourTemperature -= colourTemperatureSpeed * dayNightSpeed * Time.deltaTime;
                }
                IntensityController();
            } 
            else if(time >= 255f && time <= 359f)
            {
                colourTemperature -= colourTemperatureSpeed * dayNightSpeed * Time.deltaTime;
                exposure.compensation.value = Mathf.Clamp(dayNightSpeed * Time.deltaTime, minExposure, maxExposure);
            }
            else
            {
                lightIntensity += lightIntensitySpeed * dayNightSpeed * Time.deltaTime;
                IntensityController();
            }
           
        }

        private void IntensityController()
        {
            lightDataHD.intensity = Mathf.Clamp(lightIntensity, minLightIntensity, maxLightIntensity);
            lightData.colorTemperature = Mathf.Clamp(colourTemperature, 0, maxColourTemperature);
        }

        void DayTime()
        {

            transform.Rotate(dayNightSpeed * Time.deltaTime, 0, 0);
     
        }
        void NightTime()
        {
            transform.Rotate(dayNightSpeed * Time.deltaTime, 0, 0);
        }
    }

}
