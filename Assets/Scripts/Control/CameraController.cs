using UnityEngine;
using Cinemachine;

namespace ASRPG.Control {
public class CameraController : MonoBehaviour
    {
        CinemachineVirtualCamera cameraBase;
        CinemachineFramingTransposer camera;

        [SerializeField] float zoomMin = 5f;
        [SerializeField] float zoomMax = 15f;
        [SerializeField] float zoomPower = 1f;
        private void Start()
        {
            cameraBase = GetComponent<CinemachineVirtualCamera>();
            camera = cameraBase.GetCinemachineComponent<CinemachineFramingTransposer>();
        }
        private void Update() 
        {
            if(Input.mouseScrollDelta.y > 0f)
            {
                camera.m_CameraDistance = Mathf.Clamp(camera.m_CameraDistance -= zoomPower, zoomMin, zoomMax);
 
            }
            else if (Input.mouseScrollDelta.y < 0f)
            {
                camera.m_CameraDistance = Mathf.Clamp(camera.m_CameraDistance += zoomPower, zoomMin, zoomMax);
            }
        }
    }
}
