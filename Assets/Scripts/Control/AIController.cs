using UnityEngine;
using ASRPG.Combat;
using ASRPG.Core;
using ASRPG.Movement;
using System;

namespace ASRPG.Control
{
    public class AIController : MonoBehaviour
    {
        [SerializeField] float chaseDistance = 10f; 
        [SerializeField] float suspicionTime = 3f;
        [SerializeField] PatrolPath patrollingPath;
        [SerializeField]float waypointTolerance = 1f;
        [SerializeField]float waypointDwellingTimer = 5f;
        Fighter fighter;
        Health health;
        GameObject player;

        Vector3 guardOriginLocation;
        float timeSincePlayerSpotted = Mathf.Infinity;
        float timeAtWaypoint= Mathf.Infinity;
        int currentWaypointIndex = 0;

        private void Start() {
          fighter = GetComponent<Fighter>();
          player = GameObject.FindWithTag("Player");
          health = GetComponent<Health>();
          guardOriginLocation = transform.position;
        }
        private void Update() {

            if (health.IsDead()) return;
            if(player == null) return;

            if(WithinRangeToAttack()  && fighter.CanAttack(player))
            {
                timeSincePlayerSpotted = 0;
                AttackBehaviour();
            }
            else if(timeSincePlayerSpotted < suspicionTime)
            {
                SuspicionBehaviour();
            }
            else
            {
                PatrolBehaviour();
            }
            timeSincePlayerSpotted += Time.deltaTime;
        }

        private void PatrolBehaviour()
        {
            Vector3 nextPatrolPoint = guardOriginLocation;
            if (patrollingPath != null)
            {
                if (AtWaypoint())
                {
                    timeAtWaypoint = 0;
                    CycleWaypoint();
                }
                nextPatrolPoint = GetCurrentWaypoint();
            }
            fighter.Cancel();
            WaypointDwelling(nextPatrolPoint);
            timeAtWaypoint += Time.deltaTime;
        }

        private void WaypointDwelling(Vector3 nextPatrolPoint)
        {
            if (timeAtWaypoint > waypointDwellingTimer)
            {
                GetComponent<Mover>().StartMoveAction(nextPatrolPoint);
            }
        }

        private void CycleWaypoint()
        {
            currentWaypointIndex = patrollingPath.GetNextIndex(currentWaypointIndex);
        }

        private Vector3 GetCurrentWaypoint()
        {
            return patrollingPath.GetWaypoint(currentWaypointIndex);
        }

        private bool AtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void SuspicionBehaviour()
        {
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void AttackBehaviour()
        {
            fighter.Attack(player);
        }

        private bool WithinRangeToAttack()
        {
            return Vector3.Distance(player.transform.position, transform.position) < chaseDistance;
        }

        //Called by unity 
        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
        }
    }
}
