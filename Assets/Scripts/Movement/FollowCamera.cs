﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ASRPG.Movement {
public class FollowCamera : MonoBehaviour
    {
        [SerializeField] Transform target;

        void LateUpdate()
        {
            gameObject.transform.position = new Vector3(target.position.x, target.position.y, target.position.z);
        }
    }

}
