﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ASRPG.Core
{
    public class DestroyAfterEffect : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            if (!GetComponent<ParticleSystem>().IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }

}
