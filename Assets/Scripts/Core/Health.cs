using UnityEngine;
using ASRPG.Core;

namespace ASRPG.Core {
    public class Health : MonoBehaviour
    {
        [SerializeField] float health = 100f;
        bool isDead = false;

        public bool IsDead()
        {
            return isDead;
        }
        public void TakeDamage(float damage) 
        {
            health = Mathf.Max(health - damage, 0);
            if (health <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            if (isDead == true) return;
            isDead = true;
            GetComponent<Animator>().SetTrigger("isDead");
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }
    }
}
